﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.Models
{
    [ComplexType]
    public class Address
    {
        [Display(Name = "State")]
        public string state { get; set; }
        [Display(Name = "City")]
        public string city { get; set; }
        [Display(Name = "Street")]
        public string street { get; set; }
        [Display(Name = "Building Number")]
        public int bldgnumber { get; set; }
        [Display(Name = "Zip Code")]
        public int zip { get; set; }
    }

}
