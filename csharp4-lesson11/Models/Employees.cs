﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace csharp4_lesson11.Models
{
    public class Employees
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Employee Name")]        
        public Name EmployeeName { get; set; }
        [Display(Name = "Wage")]
        public double wage { get; set; }
        [Display(Name = "Position")]
        public string Position { get; set; }
        public int? CompanyId { get; set; }
        public virtual Company company { get; set; }

    }
}
