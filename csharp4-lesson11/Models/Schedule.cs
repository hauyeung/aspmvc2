﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.Models
{
    public class Schedule
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Scheduled Date")]
        public string scheduleddate { get; set; }
        [Display(Name = "Scheduled Time")]
        public string scheduletime { get; set; }
        [Display(Name = "Customer Name")]
        public string customername { get; set; }
        [Display(Name = "Paid")]
        public bool servicepaid { get; set; }
        public virtual int serviceid { get; set; }
        public virtual Company company { get; set; }
        public int? CompanyId { get; set; }
    }
}
