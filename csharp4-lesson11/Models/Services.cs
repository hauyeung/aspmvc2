﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace csharp4_lesson11.Models
{
    public class Services
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Price Per Service")]
        public double priceperservice { get; set; }
        [Display(Name = "Type of Service")]
        public string typeofservice { get; set; }
        public virtual Company company { get; set; }
        public int? CompanyId { get; set; }
    }
}
