﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.Models
{

    public class Company
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Company Name")]
        public string nameofcompany { get; set; }
        public virtual List<Employees> listofemployees { get; set; }
        public virtual List<Services> listofservices { get; set; }
        public virtual List<Schedule> listofschedules { get; set; }
        [Display(Name = "Phone Number")]
        public string phonenumber { get; set; }
        
        public Address address { get; set; }

    }
}
