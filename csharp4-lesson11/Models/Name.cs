﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.Models
{
    [ComplexType]
    public class Name
    {
        [Display(Name = "First Name")]
        public string first { get; set; }
        [Display(Name = "Middle Name")]
        public string middle { get; set; }
        [Display(Name = "Last Name")]
        public string last { get; set; }
        [Display(Name = "Full Name")]
        public string full { get; set; }
    }
}
