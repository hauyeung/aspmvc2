﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson11.Models;
using System.Data;
using System.Data.Entity;
using csharp4_lesson11.AbstractionLayer;
namespace csharp4_lesson11.Controllers
{
    public class ScheduleController : Controller
    {
        private LawnContext DBsch = new LawnContext();
        private LawnRepository lawn = new LawnRepository(new LawnContext());
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ScheduleList()
        {
            return View(DBsch.schedules.ToList());
        }

        //
        // GET: /CdDBsch/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CdDBsch/Create
        [AcceptVerbs(HttpVerbs.Post), ActionName("Create")]
        public ActionResult Create([Bind(Exclude = "ID")] Schedule schToCreate)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            DBsch.schedules.Add(schToCreate);
            DBsch.SaveChanges();
            return RedirectToAction("ScheduleList");
        }

        //
        // GET: /CdDBsch/Edit/
        public ActionResult Edit(int id = 0)
        {
            Schedule item = DBsch.schedules.Find(id);
            if (item == null)
            {
                return RedirectToAction("ScheduleList");
            }
            return View(item);
        }
        //
        // POST: /CdDBsch/Edit/ 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Schedule cdDBschToEdit)
        {
            if (!ModelState.IsValid)
            {
                return View(cdDBschToEdit);
            }
            DBsch.Entry(cdDBschToEdit).State = EntityState.Modified;
            DBsch.SaveChanges();
            return RedirectToAction("ScheduleList");
        }
        //
        // GET: /CdDBsch/Delete/
        public ActionResult Delete(int? id)
        {
            Schedule item = DBsch.schedules.Find(id);
            if (item == null)
            {
                return RedirectToAction("ScheduleList");
            }
            DBsch.schedules.Remove(item);
            DBsch.SaveChanges();
            return RedirectToAction("ScheduleList");
        }

        //
        // POST: /CdDBsch/Delete/ 
        [AcceptVerbs(HttpVerbs.Post), ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Schedule item = DBsch.schedules.Find(id);
            DBsch.schedules.Remove(item);
            DBsch.SaveChanges();
            return RedirectToAction("ScheduleList");
        }

        public ActionResult Details(int i)
        {
            return View(DBsch.schedules.Find(i));
        }

        public ActionResult Index(int? i)
        {
            return View(DBsch.schedules.ToList());
        }

        public ActionResult GetSchedule()
        {
            return View(lawn.GetScheduleList());
        }

    }
}
