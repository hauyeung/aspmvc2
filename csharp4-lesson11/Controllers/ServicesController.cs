﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson11.Models;
using System.Data;
using System.Data.Entity;
using csharp4_lesson11.AbstractionLayer;
namespace csharp4_lesson11.Controllers
{
    public class ServicesController : Controller
    {
        private LawnContext Dbsvc = new LawnContext();
        private LawnRepository lawn = new LawnRepository(new LawnContext());

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ServicesList()
        {
            return View(Dbsvc.services.ToList());
        }
        //
        // GET: /CdDbsvc/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CdDbsvc/Create
        [AcceptVerbs(HttpVerbs.Post), ActionName("Create")]
        public ActionResult Create([Bind(Exclude = "ID")] Services svcToCreate)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            Dbsvc.services.Add(svcToCreate);
            Dbsvc.SaveChanges();
            return RedirectToAction("ServicesList");
        }

        //
        // GET: /CdDbsvc/Edit/
        public ActionResult Edit(int id = 0)
        {
            Services item = Dbsvc.services.Find(id);
            if (item == null)
            {
                return RedirectToAction("ServicesList");
            }
            return View(item);
        }
        //
        // POST: /CdDbsvc/Edit/ 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Services cdDbsvcToEdit)
        {
            if (!ModelState.IsValid)
            {
                return View(cdDbsvcToEdit);
            }
            Dbsvc.Entry(cdDbsvcToEdit).State = EntityState.Modified;
            Dbsvc.SaveChanges();
            return RedirectToAction("ServicesList");
        }
        //
        // GET: /CdDbsvc/Delete/
        public ActionResult Delete(int? id)
        {
            Services item = Dbsvc.services.Find(id);
            if (item == null)
            {
                return RedirectToAction("ServicesList");
            }
            Dbsvc.services.Remove(item);
            Dbsvc.SaveChanges();
            return RedirectToAction("ServicesList");
        }

        //
        // POST: /CdDbsvc/Delete/ 
        [AcceptVerbs(HttpVerbs.Post), ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Services item = Dbsvc.services.Find(id);
            Dbsvc.services.Remove(item);
            Dbsvc.SaveChanges();
            return RedirectToAction("ServicesList");
        }

        public ActionResult Details(int i)
        {
            return View(Dbsvc.services.Find(i));
        }

        public ActionResult GetServices()
        {
            return View(lawn.GetServicesList());
        }
    }
}
