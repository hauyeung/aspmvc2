﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson11.Models;
using System.Data;
using System.Data.Entity;
using csharp4_lesson11.AbstractionLayer;
namespace csharp4_lesson11.Controllers
{
    public class EmployeesController : Controller
    {
        private LawnContext DB = new LawnContext();
        private LawnRepository lawn = new LawnRepository(new LawnContext());
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Employees()
        {
            return View(DB.employees.ToList());
        }
        //
        // GET: /CdDb/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CdDb/Create
        [AcceptVerbs(HttpVerbs.Post), ActionName("Create")]
        public ActionResult CreateEmployee([Bind(Exclude = "ID")] Employees empToCreate)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            DB.employees.Add(empToCreate);
            DB.SaveChanges();
            return RedirectToAction("EmployeesList");
        }

        //
        // GET: /CdDb/Edit/
        public ActionResult Edit(int id = 0)
        {
            Employees item = DB.employees.Find(id);
            if (item == null)
            {
                return RedirectToAction("EmployeesList");
            }
            return View(item);
        }
        //
        // POST: /CdDb/Edit/ 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Employees cddbToEdit)
        {
            if (!ModelState.IsValid)
            {
                return View(cddbToEdit);
            }
            DB.Entry(cddbToEdit).State = EntityState.Modified;
            DB.SaveChanges();
            return RedirectToAction("EmployeesList");
        }
        //
        // GET: /CdDb/Delete/
        public ActionResult Delete(int? id)
        {
            Employees item = DB.employees.Find(id);
            if (item == null)
            {
                return RedirectToAction("EmployeesList");
            }
            DB.employees.Remove(item);
            DB.SaveChanges();
            return RedirectToAction("EmployeesList");
        }

        //
        // POST: /CdDb/Delete/ 
        [AcceptVerbs(HttpVerbs.Post), ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Employees item = DB.employees.Find(id);
            DB.employees.Remove(item);
            DB.SaveChanges();
            return RedirectToAction("EmployeesList");
        }

        public ActionResult Details(int i)
        {
            return View(lawn.GetEmployee(i));
        }

        public ActionResult EmployeesList(int? i)
        {
            return View(DB.employees.ToList());
        }

        public ActionResult GetEmpByWage(double a, double b)
        {
            var result = lawn.GetEmployeesListByWage(a, b);
            return View(result);
        }

        public ActionResult GetEmps()
        {
            return View(lawn.GetEmployeesList());
        }

        public ActionResult GetEmployee(int id)
        {
            return View(lawn.GetEmployee(id));
        }
   
    }
}
