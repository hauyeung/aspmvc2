﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson11.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace csharp4_lesson11.Models
{
    public class LawnContext : DbContext
    {
        public DbSet<Employees> employees { get; set; }
        public DbSet<Services> services { get; set; }
        public DbSet<Company> companies { get; set; }
        public DbSet<Schedule> schedules { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Employees>().HasOptional(a => a.company).WithMany(b => b.listofemployees).HasForeignKey(a => a.CompanyId);
            modelBuilder.Entity<Services>().HasOptional(c => c.company).WithMany(d => d.listofservices).HasForeignKey(c => c.CompanyId);
            modelBuilder.Entity<Schedule>().HasOptional(e => e.company).WithMany(f => f.listofschedules).HasForeignKey(e => e.CompanyId);
        }
    }
}
