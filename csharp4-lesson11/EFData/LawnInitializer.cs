﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using csharp4_lesson11.Models;

namespace AspNetDatabases.Databases
{
    public class LawnInitializer : DropCreateDatabaseIfModelChanges<LawnContext>
    {
        protected override void Seed(LawnContext context)
        {

            var company = new List<Company>
            {
                new Company{nameofcompany="Company", phonenumber="11111", address=new Address(){state="Washington", city="Washington", street="1 1st St", bldgnumber=1, zip=00000}},
            };
            company.ForEach(m => context.companies.Add(m));
            context.SaveChanges();

            var employees = new List<Employees>
            {
                new Employees{EmployeeName = new Name(){ first = "John" ,middle="A", last="Smith", full="John A Smith"}, wage = 50, Position = "Technician", CompanyId=1 },
            };
            employees.ForEach(m => context.employees.Add(m));
            context.SaveChanges();

            var services = new List<Services>
            {
                new Services{priceperservice=50, typeofservice="Work", CompanyId=1},
            };
            services.ForEach(m => context.services.Add(m));
            context.SaveChanges();

            var schedule = new List<Schedule>
            {
                new Schedule{scheduleddate="1/1/2000",scheduletime="1:00", customername="Bob", servicepaid=true, serviceid=1, CompanyId=1},
            };
            schedule.ForEach(m => context.schedules.Add(m));
            context.SaveChanges();
        }
        
    }
}