﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.AbstractionLayer
{
    public class LawnRepository : ILawnRepository, IDisposable
    {
        private LawnContext l;
        public LawnRepository(LawnContext context)
        {
            this.l = context;
        }

        public IEnumerable<Employees> GetEmployeesList()
        {
            return l.employees.ToList();
        }

        public IEnumerable<Employees> GetEmployeesListByWage(double a, double b)
        {
            return l.employees.Where(x=>x.wage >= a).Where(x=>x.wage <= b);
        }

        public IEnumerable<Employees> GetEmployee(int id)
        {
            return l.employees.Where(x => x.Id==id);
        }


        public IEnumerable<Schedule> GetScheduleList()
        {
            return l.schedules.ToList();
        }

        public IEnumerable<Services> GetServicesList()
        {
            return l.services.ToList();
        }

        public void Dispose()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }

    }
}
