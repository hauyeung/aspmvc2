﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.AbstractionLayer
{
    public class LawnWorkUnit : IDisposable
    {
        private LawnContext lawn = new LawnContext();
        private LawnRepository lawnrepo;

        public LawnRepository LawnRepository
        {
            get
            {
                if (this.lawnrepo == null)
                {
                    this.lawnrepo = new LawnRepository(lawn);
                }
                return lawnrepo;
            }
        }

        public void Save()
        {
            lawn.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool Disposal)
        {
            if (!this.disposed)
            {
                if (Disposal)
                {
                    lawn.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            lawn.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
