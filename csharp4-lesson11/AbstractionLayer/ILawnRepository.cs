﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson11.Models;

namespace csharp4_lesson11.AbstractionLayer
{
    public interface ILawnRepository: IDisposable 
    {
        IEnumerable<Employees> GetEmployeesList();
        IEnumerable<Employees> GetEmployeesListByWage(double a, double b);
        IEnumerable<Employees> GetEmployee(int id);
        IEnumerable<Schedule> GetScheduleList();
        IEnumerable<Services> GetServicesList();
    }

}

